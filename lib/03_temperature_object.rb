require 'byebug'
class Temperature

  # TODO: your code goes here!
  def initialize (options = {})
    @options = options
  end

  def in_fahrenheit
  #  debugger
    if @options.keys[0] == :c
    return  @options.values[0]  * 9/5.0 + 32
    else
    return  @options.values[0]
    end
  end

  def in_celsius
    if @options.keys[0] == :f
      ((@options.values[0] -32) *5/9.0 )
    else
      @options.values[0]
    end
  end

  def self.from_celsius(deg)
    self.new(c: deg)

  end

  def self.from_fahrenheit(deg)
    self.new(f: deg)
  end
end


class Celsius < Temperature
    def initialize(temp)
      super({c: temp})
    end

  end

  class Fahrenheit < Temperature
    def initialize(temp)
      super({f: temp})
    end
  end
