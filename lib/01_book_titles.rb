require 'byebug'
class Book
  # TODO: your code goes here!

  attr_accessor :title
  # def initialize(title)
  #   @title = title
  # end

  def title
    prepositions = ["and","of","the","in","a","an"]
    words = @title.split
    ans = []
    words.each_with_index do |word,idx|
       if prepositions.include?(word) && idx != 0
         ans << word
         ans << " "
         next
       end
      word.chars.map.with_index do |el,idx|
        if idx == 0
          ans << el.upcase
        else
          ans << el
        end
      end
      ans << " "
    end
    ans.delete_at(-1)
    ans.join
  end
end
